#include<iostream>
#include<string>
#include<cstdlib>
#include<list>
using namespace std;

class Student {
public:
	int id;
	string name;
	Student() {
		id = 0;
		name = "";
	}
};

int HashById(int);
int HashByName(string toHash);
list<Student> id_table[50]; // store hash id in the list
list<Student> name_table[50]; // store hash name in the list
void inputData(); // for input name and id into both list
void id_search(); //search by id
void name_search(); //search by name
void example_data(); //generates example data for test
bool compareString(string, string); //compare 2 strings to use in name_search
void run();

int main() {

	example_data();
	run();
	return 0;
}

void example_data() { //generate test case data 
	Student ISNE_data;

	ISNE_data.id = 560611002;
	ISNE_data.name = "Phone";
	id_table[HashById(ISNE_data.id)].push_back(ISNE_data);
	name_table[HashByName(ISNE_data.name)].push_back(ISNE_data);

	ISNE_data.id = 570611001;
	ISNE_data.name = "Doi";
	id_table[HashById(ISNE_data.id)].push_back(ISNE_data);
	name_table[HashByName(ISNE_data.name)].push_back(ISNE_data);

	ISNE_data.id = 570611001;
	ISNE_data.name = "Khao";
	id_table[HashById(ISNE_data.id)].push_back(ISNE_data);
	name_table[HashByName(ISNE_data.name)].push_back(ISNE_data);

	ISNE_data.id = 580611001;
	ISNE_data.name = "Friend";
	id_table[HashById(ISNE_data.id)].push_back(ISNE_data);
	name_table[HashByName(ISNE_data.name)].push_back(ISNE_data);

	ISNE_data.id = 580611020;
	ISNE_data.name = "Dream";
	id_table[HashById(ISNE_data.id)].push_back(ISNE_data);
	name_table[HashByName(ISNE_data.name)].push_back(ISNE_data);

	ISNE_data.id = 580611041;
	ISNE_data.name = "Earth";
	id_table[HashById(ISNE_data.id)].push_back(ISNE_data);
	name_table[HashByName(ISNE_data.name)].push_back(ISNE_data);

	ISNE_data.id = 580611002;
	ISNE_data.name = "Yo";
	id_table[HashById(ISNE_data.id)].push_back(ISNE_data);
	name_table[HashByName(ISNE_data.name)].push_back(ISNE_data);

}
int HashById(int value) { // Hash by using ID number
	return value % 50;
}
int HashByName(string toHash) {//Hash by using name
	int hashValue = 0;
	for (unsigned int Pos = 0; Pos < toHash.length(); Pos++) {
		hashValue = hashValue + toupper(toHash.at(Pos));
	}
	return (hashValue % 50);
}
void inputData() { //input data

	Student ISNE_data;
	int Id;
	string name;
	cout << "Input student ID : ";
	cin >> Id;
	cout << "Input student name : ";
	cin >> name;

	ISNE_data.id = Id;
	ISNE_data.name = name;

	id_table[HashById(Id)].push_back(ISNE_data); // store to list id
	name_table[HashByName(name)].push_back(ISNE_data); // store to list name
}
void id_search() {
	cout << "Search by ID : ";
	int keyword_ID;
	cin >> keyword_ID;
	bool found = false;

	for (list<Student>::iterator it = id_table[HashById(keyword_ID)].begin(); it != id_table[HashById(keyword_ID)].end(); it++) {
		if (it->id == keyword_ID) { //found!
			cout << endl << "Result :" << endl;
			cout << it->id << " : " << it->name << endl;
			found = true;
		}
	}

	if (found == false) {
		cout << keyword_ID << " is not found." << endl;
	}

}

void name_search() {
	cout << "Search by Name : ";
	string keyword_name;
	cin >> keyword_name;
	bool found = false;
	for (list<Student>::iterator it = name_table[HashByName(keyword_name)].begin(); it != name_table[HashByName(keyword_name)].end(); it++) {
		if (compareString(it->name, keyword_name)) { //compareString
			cout << endl << "Result :" << endl;
			cout << it->id << " : " << it->name << endl;
			found = true;
		}
	}
	if (found == false) {
		cout << keyword_name << " is not found." << endl;
	}
}

bool compareString(string string1, string string2) {//campare string
	int size1 = string1.length();
	int size2 = string2.length();
	int for_check = 0;
	if (size1 != size2) {
		return false;
	}
	else {
		for (int i = 0; i < size1; i++) {
			if (toupper(string1[i]) == toupper(string2[i]))
			{
				for_check++;
			}
		}
		if (for_check == size1) {
			return true;
		}
		return false;
	}
}

void run() {
	int press = 1;;
	while (press != 0) {
		cout << endl;
		cout << "Input 1 to add new data " << endl << "Input 2 to search by id " << endl << "Input 3 to search by name " << endl << "Input 0 to exit " << endl << "Input : ";
		cin >> press;
		switch (press) {
		case 0: break;
		case 1: inputData(); break;
		case 2: id_search(); break;
		case 3: name_search(); break;
		default:
			cout << "Wrong input, please try again dude.";
		}
	}
	cout << "End program" << endl;
}